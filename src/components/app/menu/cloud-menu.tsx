import { CircularProgress, IconButton, Menu, MenuItem, Tooltip } from '@material-ui/core';
import { CloudDone, CloudOff, CloudQueue } from '@material-ui/icons';
import React, { Fragment, memo, useState } from 'react';
import CryptoService from '../../../services/crypto-service';
import DataService from '../../../services/data-service';
import StorageHandler from '../../../storage/storage-handler';
import SupportedClouds, { CloudsMeta } from '../../../types/supported-clouds';
import './menu.scss';

interface CloudMenuProps {
  /** Whether data is currently saved to the cloud */
  saving: boolean | 'error';

  /** Called when the user disconnects his cloud storage */
  onDisconnect?: () => void;
}


/**
 * Popup menu that lists the available cloud storages, or the currently connected cloud
 */
const CloudMenu: React.FC<CloudMenuProps> = ({ saving, onDisconnect }) => {
  /** Anchor element for the popup menu */
  const [anchor, setAnchor] = useState<HTMLElement | null>(null);

  /**
   * Disconnect from the current cloud and clear the cached data
   */
  const disconnect = (): void => {
    setAnchor(null);
    CryptoService.disable();
    DataService.clearCache();
    StorageHandler.disconnectCloud();
    if (onDisconnect instanceof Function) onDisconnect();
  };

  /**
   * Generate a MenuItem for the specified cloud variant
   */
  const getMenuItem = (variant: SupportedClouds, off = false): JSX.Element => (
    <MenuItem
      key={variant}
      disabled={off || !CloudsMeta[variant].configured}
      onClick={() => StorageHandler.connectCloud(variant)}
    >
      <div className="menu-icon">
        <img src={CloudsMeta[variant].logo} alt={CloudsMeta[variant].name} />
      </div>
      <span>
        {CloudsMeta[variant].name}
        {!CloudsMeta[variant].configured && <span className="sup warn">not available</span>}
      </span>
    </MenuItem>
  );

  return (
    <Fragment>
      <Tooltip title={!StorageHandler.cloud && !saving ? 'Connect Your Cloud' : ''}>
        <IconButton color="inherit" onClick={e => { if (!saving) setAnchor(e.currentTarget); }}>
          {!StorageHandler.cloud && !saving && ( // if no cloud has been connected yet
            <CloudQueue />
          )}

          {StorageHandler.cloud && !saving && ( // if a cloud is connected and idling
            <CloudDone />
          )}

          {saving && ( // if data is currently written to the cloud
            <CircularProgress color="secondary" size={24} />
          )}
        </IconButton>
      </Tooltip>
      <Menu
        className="menu-popup"
        anchorEl={anchor}
        open={!!anchor}
        onClose={() => setAnchor(null)}
        keepMounted
      >
        {!StorageHandler.cloud && ( // if no cloud has been connected yet; selection of clouds
          <div>{Object.keys(CloudsMeta).map((i, variant) => getMenuItem(variant))}</div>
        )}

        {StorageHandler.cloud && ( // if a cloud is connected
          <div>
            {getMenuItem(StorageHandler.cloud.variant, true)}
            <MenuItem onClick={disconnect}>
              <CloudOff />
              <span>Disconnect</span>
            </MenuItem>
          </div>
        )}
      </Menu>
    </Fragment>
  );
};

export default memo(CloudMenu);
