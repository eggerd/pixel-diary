import { Button, Dialog, DialogActions, DialogContent, DialogTitle, FilledInput, FormControl, InputLabel, MenuItem, Select, TextField } from '@material-ui/core';
import React, { memo, useState } from 'react';
import styled from 'styled-components';
import IDay from '../../../types/day';
import Moods from '../../../types/moods';
import './details.scss';

/** Parameters for Date().toLocaleDateString(locale, options) */
const dateString = {
  locale: 'en-US',
  options: { weekday: 'long', month: 'long', day: 'numeric' },
};

/** Amount of characters that the note is allowed to have */
const maxNoteLength = 140;

interface DayDetailsProps {
  /** Date of the day that is being edited */
  date: Date;

  /** State data of that day */
  values: IDay;

  /**
   * Called whenever the dialog has been closed
   * @param values the new state data; only passed if the user closed the dialog by pressing 'apply'
   */
  onClose: (values?: IDay) => void;
}


/**
 * Dialog for editing the properties of a day
 */
const DayDetails: React.FC<DayDetailsProps> = ({ date, values, onClose }) => {
  /** set mood if not present (new day), so that a mood is pre selected in the dropdown */
  const [inputData, setInputData] = useState<IDay>(values.mood ? values : { ...values, mood: 0 });

  /**
   * Handles the change of an input field. The field requires a name, which will be used as key for
   * the state object
   */
  const handleChange = (event: React.ChangeEvent<{ name?: string; value: any }>): void => {
    const { name } = event.target;
    if (!name) return;

    let { value } = event.target;
    if (typeof inputData[name] === 'number') { // if interface type of corresponding field is numeric
      value = parseInt(value, 10);
    } else {
      value = value ? value.substr(0, maxNoteLength) : undefined;
    }

    setInputData(oldInputData => ({ ...oldInputData, [name]: value }));
  };

  /** Div which is colored based on the mood that is currently selected in the dropdown */
  const MoodPreview = styled.div`
    background-color: ${Moods[inputData.mood || 0].color};
  `;

  return (
    <Dialog id="day-details" className="day-details" open onClose={() => onClose()}>
      <DialogTitle>{date.toLocaleDateString(dateString.locale, dateString.options)}</DialogTitle>
      <DialogContent>
        <div className="mood-selection">
          <MoodPreview />
          <FormControl variant="filled" fullWidth>
            <InputLabel htmlFor="mood-select">Mood</InputLabel>
            <Select
              value={inputData.mood || 0}
              onChange={handleChange}
              input={<FilledInput name="mood" id="mood-select" />}
            >
              {Moods.map((v, i) => {
                const MoodColor = styled.div`
                  background-color: ${v.color};
                  height: 80%;
                `;

                return (
                  <MenuItem className="mood-menu-item" key={v.color} value={i}>
                    <MoodColor />
                    <span>{v.name}</span>
                  </MenuItem>
                );
              })}
            </Select>
          </FormControl>
        </div>
        <TextField
          id="day-details-note"
          name="note"
          label="Note"
          multiline
          rows="4"
          fullWidth
          margin="normal"
          variant="filled"
          value={inputData.note || ''}
          onChange={handleChange}
          helperText={`${(inputData.note || '').length} / ${maxNoteLength}`}
        />
      </DialogContent>
      <DialogActions>
        <Button id="day-details-apply" onClick={() => onClose(inputData)} color="primary">Apply</Button>
        <Button id="day-details-close" onClick={() => onClose()} color="primary">Close</Button>
      </DialogActions>
    </Dialog>
  );
};

export default memo(DayDetails);
