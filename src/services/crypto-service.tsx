import CryptoJS from 'crypto-js';
import { bugsnagClient } from '../helper/bugsnag';
import { CryptoError } from '../types/errors';

/**
 * Manages all encryption related operations
 */
export default abstract class CryptoService {
  /**
   * This value is used to easily check whether the correct password has been provided or not. When
   * setting up encryption for the first time, the service will encrypt this value with the provided
   * password (referred to as "checkCipher"). If the checkCipher is passed to the service during
   * init, it will try to decrypt it and compare the result with this value, to determine if the
   * decryption was successful.
   *
   * IMPORTANT: Changing this value will break all existing encryptions, by locking the users out of
   * their diaries. So, never change it, there is no need to anyway
   */
  private static readonly checkValue = '4c6f72656d20697073756d';

  /** The hashed password that is used for all encryptions/decryptions */
  private static passphrase: string | false = false;

  /**
   * Sets the passed password as passphrase for the encryption. If the checkCipher is passed, it
   * will validate that the passed password can be used to decrypt it, and will only then set it as
   * passphrase
   */
  static init(password: string, checkCipher?: string): string | false {
    if (!this.passphrase) {
      const passwordHash = CryptoJS.SHA256(password).toString();
      if (checkCipher) {
        try {
          const value = CryptoJS.AES.decrypt(checkCipher, passwordHash).toString(CryptoJS.enc.Utf8);
          if (this.checkValue !== value) return false;
        } catch (e) {
          bugsnagClient.notify(e);
          return false;
        }
      }

      this.passphrase = passwordHash;
      bugsnagClient.leaveBreadcrumb('Crypto Service initialized');
    }

    return CryptoJS.AES.encrypt(this.checkValue, this.passphrase).toString();
  }

  /**
   * Encrypts the passed value symmetrically, if encryption has been enabled. Otherwise, it will
   * just return the passed plaintext value
   */
  static encrypt(value: string): string {
    if (!this.passphrase) return value;

    try {
      return JSON.stringify({
        hash: CryptoJS.HmacSHA256(value, this.passphrase).toString(),
        cipher: CryptoJS.AES.encrypt(value, this.passphrase).toString(),
      });
    } catch (e) {
      console.error('Encryption failed: ', value, e);
      throw new CryptoError();
    }
  }

  /**
   * Decrypts the passed value, if encryption has been enabled. Otherwise, it will just return the
   * passed value. In case the passed value isn't a crypto tuple (an object consisting of a hash
   * and the cipher) the passed value will be returned, too
   */
  static decrypt(value: string): string {
    if (!this.passphrase) return value;

    try {
      const tuple = JSON.parse(value);
      if (!tuple.hash || !tuple.cipher) return value; // not a crypto tuple, if those are missing

      const plain = CryptoJS.AES.decrypt(tuple.cipher, this.passphrase).toString(CryptoJS.enc.Utf8);
      const hash = CryptoJS.HmacSHA256(plain, this.passphrase).toString();
      if (tuple.hash !== hash) throw new Error('Hash Mismatch');
      return plain;
    } catch (e) {
      console.error('Decryption failed: ', e);
      throw new CryptoError();
    }
  }

  /**
   * Disables the encryption
   */
  static disable(): void {
    this.passphrase = false;
  }

  /**
   * Whether encryption is currently enabled or not
   */
  static isEnabled(): boolean {
    return !!this.passphrase;
  }
}
