import React, { Fragment } from 'react';

/* eslint-disable */
const privacyInfo = (
  <Fragment>
    <p>
      This website does not use cookies and does not collect any kind of personal information about
      its visitors. All information entered on this website, for example a mood or a note (hereafter
      called "Diary Content"), is stored in the integrated storage of the visitors browser, unless
      the visitor chooses to connect a cloud storage (see "Cloud Storage").
    </p>

    <h4>Information Collected for Error Reporting</h4>
    <p>
      This website uses <a href="https://www.bugsnag.com/" target="_blank">Bugsnag</a> to collect
      error reports. These error reports include basic non-personally-identifying information from
      website visitors of the sort that web browsers typically make available, such as the browser
      type and version, name of the operating system, language preference, and the date and time the
      error occurred.<br />
      <br />
      However, Bugsnag might collect additional information when connecting to their servers. Details
      can be found in their Privacy Policy at <a href="https://docs.bugsnag.com/legal/privacy-policy/" target="_blank">
      docs.bugsnag.com/legal/privacy-policy</a>.
    </p>

    <h4>Information Collected by the Hoster</h4>
    <p>
      This website is hosted using a service of <a href="https://about.gitlab.com/" target="_blank">
      GitLab B.V.</a> ("GitLab"). Like most hosters, GitLab collects basic non-personally-identifying
      information from website visitors of the sort that web browsers and servers typically make
      available, such as the browser type, language preference, referring site, and the date and
      time of each visitor request.<br />
      <br />
      GitLab also collects potentially personally-identifying information like Internet Protocol
      (IP) addresses from visitors. GitLab does not use such information to identify or track
      individual visitors, however.<br />
      <br />
      Further details can be found in GitLabs Privacy Policy at <a href="https://about.gitlab.com/privacy/" target="_blank">
      about.gitlab.com/privacy</a>.
    </p>

    <h4>Cloud Storage</h4>
    <p>
      In case the visitor chooses to connect a cloud storage, all Diary Content will be saved on
      the servers of said cloud storage, and the Privacy Policy of the respective provider applies:
    </p>
    <ul>
      <li>Dropbox - <a href="https://www.dropbox.com/privacy" target="_blank">dropbox.com/privacy</a></li>
      <li>OneDrive - <a href="https://privacy.microsoft.com/privacystatement" target="_blank">privacy.microsoft.com/privacystatement</a></li>
      <li>Google Drive - <a href="https://policies.google.com/privacy" target="_blank">policies.google.com/privacy</a></li>
    </ul>
  </Fragment>);

export default privacyInfo;
