# Pixel Diary

[![version](https://img.shields.io/badge/dynamic/json.svg?label=version&url=https%3A%2F%2Fgitlab.com%2Fapi%2Fv4%2Fprojects%2F11318097%2Frepository%2Ftags&query=%24%5B0%5D.name&colorB=blue)](https://gitlab.com/eggerd/pixel-diary/-/releases)
[![pipeline status](https://gitlab.com/eggerd/pixel-diary/badges/master/pipeline.svg)](https://gitlab.com/eggerd/pixel-diary/-/pipelines) 
[![quality gate](https://sonarcloud.io/api/project_badges/measure?project=pixel-diary&metric=alert_status)](https://sonarcloud.io/dashboard?id=pixel-diary) 
[![maintainability](https://sonarcloud.io/api/project_badges/measure?project=pixel-diary&metric=sqale_rating)](https://sonarcloud.io/dashboard?id=pixel-diary)


This is my first project working with [React](https://reactjs.org/). It was inspired by people posting their hand drawn *mood calendars* on [9gag](https://9gag.com/gag/an9yvEo). So the goal of this project is/was to write a small serverless website that replicates those hand drawn calendars and extends them by some additional features.

##### Features
- Serverless single page web application
- A fully responsive and modern design
- Select a mood out of 11 pre-defined combinations, for each day of the year
- Add a tweet sized comment (140 characters) to summarize your day
- Save your diary data on [Dropbox](https://dropbox.com), [OneDrive](https://onedrive.live.com), [Google Drive](https://drive.google.com), or simply in your browsers storage *
- Optionally, you can enable AES-256-CBC encryption, to protect your privacy and lock your diary

<sup>* Browser storage is limited and may vary between browsers - [more about space usage](https://gitlab.com/eggerd/pixel-diary/-/wikis#space-usage)</sup>

##### Screenshots
| ![Desktop Screenshot](https://gitlab.com/eggerd/pixel-diary/-/raw/assets/desktop.png) | ![Mobile Screenshot](https://gitlab.com/eggerd/pixel-diary/-/raw/assets/mobile.png) |
| --- | --- |


## Try It
The latest stable release is available at [pixel-diary.eggerd.de](https://pixel-diary.eggerd.de).


## Requirements
A modern browser (i.e. **not** Internet Explorer)


## Installation
If you want to host a Pixel Diary yourself, simply clone the repository or download the [latest version](https://gitlab.com/eggerd/pixel-diary/-/releases) and run:
```sh
npm install && npm run build
```

After that, serve the content of the `build` directory with any webserver you like. For example:
```sh
npx serve -l 8080 ./build
```

In case you want to host your Pixel Diary under your own domain, and not just `localhost`, and also want to use any of the supported cloud storages, you will need to register your Pixel Diary at the respective providers - instructions can be found in the [wiki](https://gitlab.com/eggerd/pixel-diary/-/wikis#cloud-storage-registration).

Moreover, Pixel Diary uses [Bugsnag](https://www.bugsnag.com/) to collect error reports; which is enabled by default. If you would like to disable the collection of error reports, simply remove the value of `REACT_APP_BUGSNAG` in `.env` and then run `npm run build` (again) to apply the new configuration.


## Licensing
This software is available freely under the MIT License.  
Copyright (c) 2019 Dustin Eckhardt
